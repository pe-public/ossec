OSSEC module
------------

The module installs OSSEC packages on Debian and Red Hat family OSes, deploys templated configuration file. The configuration options are stored in hiera and can be customized by operating system if needed. Downstream hiera overrides are also allowed to customize behavior for particular systems or groups of servers.

Default configuration of the module is taken from ISO recommendations. You probably would want to adjust it!

Note that the module enforces OSSEC installation *as local*, i.e. the logs are not being forwarded to an OSSEC server. To make use of it Splunk forwarder must be installed on the system and be configured to pick up the OSSEC logs.

## Installation

For basic use just include in your profile. 

```
include ossec
```

## Customizations

There is a number of options that can be customized, exclusions is probably the most frequently used.

#### ensure

Present by default. If set to absent uninstalls the package and removes the customized configuration files. Accumulated logs have to be removed manually.

#### scanpaths

An array of hashes determining which paths you want OSSEC to scan. Each has has the following keys:

- *`path`* - path to scan. _string_.
- *`report_changes`* - OSSEC supports sending diffs when changes are made to text files on Linux and unix systems. _yes/no_
- *`realtime`* - Realtime/continuous monitoring using the inotify system calls. _yes/no_

#### ignorepaths

An array of string paths to ignore, i.e. not to report on.

#### ignorepaths_regex

The same as above, but the paths are treated as regular expressions.

## Examples

Example of hiera configuration:

```YAML
ossec::scanpaths:
  - path: '/etc,/usr/bin,/usr/sbin'
    report_changes: 'no'
    realtime: 'yes'
  - path: '/bin,/sbin'
    report_changes: 'no'
    realtime: 'yes'
  - path: '/opt,/tmp,/var'
    report_changes: 'no'
    realtime: 'yes'

ossec::ignorepaths:
  - '/etc/mtab'
  - '/etc/hosts.deny'
  - '/etc/mail/statistics'
  - '/etc/random-seed'
  - '/etc/adjtime'
  - '/etc/httpd/logs'
  - '/var/mail'
  - '/var/opt/BESClient'

ossec::ignorepaths_regex:
  - '.*/cache'
```