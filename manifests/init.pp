# Class installs ossec to work in conjuction with Splunk.
# Splunk forwarder should be configured to pick up the
# ossec logs and push them to the Splunk indexer.

class ossec (
  Enum['present','absent'] $ensure = 'present',
  Array[Hash[String,String]] $scanpaths = [],
  Array[String] $ignorepaths = [],
  Array[String] $ignorepaths_regex = [],
){
  if $ensure == 'present' {
    $service_ensure = 'running'
    $service_enable = true
  } else {
    $service_ensure = 'stopped'
    $service_enable = false
  }

  if ($::osfamily == 'Debian') {
    include apt
    $service_name = 'ossec'

    package { 'ossec-hids-server':
      ensure  => $ensure,
      require => Class['apt']
    }

  } elsif ($::osfamily == 'RedHat') {
    include yum
    $service_name = 'ossec-hids'

    package { 'ossec-hids-server':
      ensure  => $ensure,
      require => Class['yum']
    }

  } else {
    fail('OS is not supported by ossec_iso module.')
  }

  # configuration file for ossec, templated
  file { '/var/ossec/etc/ossec.conf':
    ensure  => $ensure,
    content => template('ossec/var/ossec/etc/ossec.conf.erb'),
    owner   => 'root',
    group   => 'ossec',
    mode    => '0644',
    require => Package['ossec-hids-server'],
    notify  => Service[$service_name],
  }

  # make sure ossec runs as local rather than a server
  file_line { 'ossec-run-local':
    ensure => 'present',
    path   => '/etc/ossec-init.conf',
    match  => '^TYPE=',
    line   => 'TYPE="local"',
    notify => Service[$service_name],
  }

  service { $service_name:
    ensure  => $service_ensure,
    enable  => $servcie_enable,
    status  => 'pgrep ossec-syscheckd',
    require => [ Package['ossec-hids-server'],
                File['/var/ossec/etc/ossec.conf'], ],
  }
}
